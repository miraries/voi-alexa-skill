const got = require("got");
const faker = require("faker");

class VoiAPI {
    constructor() {
        if (!process.env.VOI_AUTH_TOKEN) {
            console.log('Authentication token not provided in environment')
            process.exit();
        }

        this.token = process.env.VOI_ACCESS_TOKEN || '';
        this.authToken = process.env.VOI_AUTH_TOKEN;

        this.headers = {
            appversion: "2.27.1",
            OS: "Android",
            model: "SM-G955N",
            brand: "Android",
            manufacturer: "samsung",
            Connection: "Keep-Alive",
            "X-Access-Token": this.token,
            "X-Request-Id": faker.random.uuid(),
            "App-Name": "Rider",
            "Content-Type": "application/json; charset=UTF-8",
            "Accept-Encoding": "gzip",
            "User-Agent": "okhttp/3.12.1"
        }

        console.log(`Instantiated voi api with access token ${this.token}`);
    }

    async acquireToken() {
        const response = await got.post("https://api.voiapp.io/v1/auth/session", {
            json: true,
            headers: this.headers,
            body: JSON.stringify({
                authenticationToken: this.authToken
            })
        });

        console.log(`Acquiring new token, status code ${response.statusCode}`);
        console.log(`New token ${response.body.accessToken}`);

        this.token = response.body.accessToken;
        this.headers["X-Access-Token"] = this.token;
    }

    async getVehicles(zone, status) {
        if (!['ready', 'riding', 'sleep'].includes(status))
            throw 'Invalid status'

        try {
            const response = await got(
                `https://api.voiapp.io/v1/vehicles/zone/${zone}/${status}`,
                {
                    json: true,
                    headers: this.headers
                }
            );

            console.log(`Fetching vehicles, status code ${response.statusCode}`);

            return response.body;
        } catch (error) {
            console.log(`Error fethcing vehicles, status code ${error.statusCode}, error ${error}`);

            if (error.response.body.code === '401.2') {
                console.log(`Acquiring new token and refetching vehicles`);
                await this.acquireToken();
                return await this.getVehicles();
            }
        }
    }

    async getZones() {
        try {
            const response = await got(
                `https://api.voiapp.io/v1/zones`,
                {
                    json: true,
                    headers: this.headers
                }
            );

            console.log(`Fetching zones, status code ${response.statusCode}`);

            return response.body.zones;
        } catch (error) {
            console.log(`Error fethcing zones, status code ${error.statusCode}, error ${error}`);

            if (error.response.body.code === '401.2') {
                console.log(`Acquiring new token and refetching zones`);
                await this.acquireToken();
                return await this.getZones();
            }
        }
    }

    async getZone({ latitude, longitude }) {
        try {
            const response = await got(
                `https://api.voiapp.io/v1/zones`,
                {
                    query: {
                        lat: latitude,
                        lng: longitude
                    },
                    json: true,
                    headers: this.headers
                }
            );

            console.log(`Fetching zones for provided location, status code ${response.statusCode}`);
            console.log(response.body);

            return response.body.zones[0].zone_id;
        } catch (error) {
            console.log(`Error fethcing zones for provided location, status code ${error.statusCode}, error ${error}`);

            if (error.response.body.code === '401.2') {
                console.log(`Acquiring new token and refetching zones for provided location`);
                await this.acquireToken();
                return await this.getZone({ latitude, longitude });
            }
        }
    }
}

module.exports = {
    VoiAPI
};
