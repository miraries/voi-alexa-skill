const zipdir = require('zip-dir');

zipdir('./', { saveTo: './build.zip', filter: (path, stat) => !/\.zip|.git$/.test(path) }, (err, buffer) => {
    if(err)
        console.log('Error building', err)
    else
        console.log('Built successfully to build.zip')
});