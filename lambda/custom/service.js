require("dotenv").config();
const got = require('got')
const { VoiAPI } = require('./voi-api')
const voiApi = new VoiAPI();
const humanizeDuration = require('humanize-duration')
const geolib = require('geolib')

const humanizeDistance = (distance) => {
    if (distance < 1000)
        return `${Math.round(100 * distance) / 100} meters`

    return `${(distance / 1000).toFixed(1)} kilometers`
}

const geocodeAddress = async (address) => {
    const query = new URLSearchParams([
        ['app_id', process.env.HERE_APP_ID],
        ['app_code', process.env.HERE_APP_CODE],
        ['searchText', address]
    ]);

    const response = await got('https://geocoder.api.here.com/6.2/geocode.json', { json: true, query });
    const { Latitude, Longitude } = response.body.Response.View[0].Result[0].Location.NavigationPosition[0];

    console.log({ address, Latitude, Longitude});

    return { latitude: Latitude, longitude: Longitude };
}

const getDistance = async (from, to) => {
    const query = new URLSearchParams([
        ['app_id', process.env.HERE_APP_ID],
        ['app_code', process.env.HERE_APP_CODE],
        ['mode', 'fastest;pedestrian;traffic:default'],
        ['waypoint0', `geo!${from.latitude},${from.longitude}`],
        ['waypoint1', `geo!${to.latitude},${to.longitude}`]
    ]);

    const response = await got('https://route.api.here.com/routing/7.2/calculateroute.json', { json: true, query });
    const summary = response.body.response.route[0].summary;

    console.log({ 'response.body': response.body });

    return summary;
}

const getClosestScooters = async (userLocation, zone) => {
    const vehicles = await voiApi.getVehicles(zone, 'ready');
    const locations = vehicles.map(v => ({ ...v, latitude: v.location[0], longitude: v.location[1] }));

    const promises = geolib.orderByDistance(userLocation, locations)
        .slice(0, 2)
        .map(async v => {
            const trip = await getDistance(userLocation, v);
            v.distance = trip.distance
            v.travelTime = trip.travelTime
            v.humanDistance = humanizeDistance(trip.distance)
            v.humanTravelTime = humanizeDuration(trip.travelTime * 1000, { round: true, delimiter: ' and ', units: ['m', 's']})
            return v;
        });

    const closestVehicles = await Promise.all(promises);

    console.log({ closestVehicles })

    return closestVehicles;
}

const formatMessage = (vehicles) => {
    let message = ''
    let [one, two] = vehicles;
    let noScooters = false

    if (vehicles[0].distance > 300){
        message += 'There are no nearby scooters. '
        noScooters = true
    }

    message += `Nearest scooter, <say-as interpret-as="spell-out">${one.short}</say-as>, 
    with ${one.battery}% battery is ${one.humanTravelTime}, or ${one.humanDistance} away. `
    message += `Second nearest, <say-as interpret-as="spell-out">${two.short}</say-as>, 
    with ${two.battery}% battery is ${two.humanTravelTime} or ${two.humanDistance} away. `
    message += noScooters ? '' : 'Enjoy your ride.'

    return message;
}

const formatAddress = (address) => {
    let a = address
    let formattedAddress = [a.addressLine1, a.postalCode, a.stateOrRegion, a.districtOrCounty, a.countryCode].filter(x => x).join(',');
    return formattedAddress;
}

const getMessage = async (address) => {
    const userAddress = formatAddress(address);
    const userLocation = await geocodeAddress(userAddress);
    const zone = await voiApi.getZone(userLocation);
    const vehicles = await getClosestScooters(userLocation, zone);
    return formatMessage(vehicles);
}

module.exports = {
    getMessage
};