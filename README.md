<h1 align="center">Welcome to voi-alexa-skill 👋</h1>

<img alt="Project banner" src="docs/banner.jpg">

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/miraries/voi-alexa-skill/-/blob/master/LICENSE" target="_blank">
    <img alt="License: GNU GPLv3" src="https://img.shields.io/badge/License-GNU GPLv3-yellow.svg" />
  </a>
</p>

> Alexa skill for finding nearby Voi scooters in the form of a Lambda function. Uses HERE Maps API for geolocation and to provide realistic walking distances. 

> The skill will detect the set user location depending on the available APIs, geocode the address if necessary using HERE API and use the Voi API to find the appropriate zone for those coordinates as well as the scooters in the zone. Scooters are then sorted by distance with `geolib` and the real walking distance is calculated for the nearest two, again using HERE API. Certain variations are made in speech depending on the available scooters to provide a better experience. 

## Install

```sh
cd lambda/custom && npm install
```

## Usage

```sh
npm run build
```

This will generate a zip file which is to be uploaded as an Lambda function in the AWS console. Connecting this function to an alexa skill requires additional setup outlined in the [Alexa developer documentation](https://developer.amazon.com/en-US/docs/alexa/custom-skills/host-a-custom-skill-as-an-aws-lambda-function.html).

## Author

👤 **Ivan Kotlaja**

* Website: ivnk.dev

## 📝 License

This project is [GNU GPLv3](https://gitlab.com/miraries/voi-alexa-skill/-/blob/master/LICENSE) licensed.